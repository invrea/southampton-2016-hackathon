# Southampton Spring 2016 Hackathon Demos and Tutorial

## Download the plugin

[Click me](https://invrea.com/plugin/excel/v1/download/).

## Tutorial

[Basic tutorial on using Invrea Excel Probabilistic Programming Plugin](https://bitbucket.org/invrea/southampton-2016-hackathon/raw/master/help/main.pdf).

## Exercises

There are three exercises that we recommend you do in the following order:

 1 [Gaussian models, part 1](https://bitbucket.org/invrea/southampton-2016-hackathon/raw/master/tutorials/simplegaussian.xlsx)

 2 [Gaussian models, part 2](https://bitbucket.org/invrea/southampton-2016-hackathon/raw/master/tutorials/sevenscientists.xlsx)

 3 [Financial model](https://bitbucket.org/invrea/southampton-2016-hackathon/raw/master/tutorials/irr.xlsx)
